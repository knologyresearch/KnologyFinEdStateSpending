
# pkgdown_build_site_Custom -----------------------------------------------

# use this to create pkgdown website
pkgdown_build_site_Custom <- function() {
  # check that datasets match data
  testthat::test_file("tests/testthat/test-dataR.R")

  # Render README.Rmd to README.md
  message("# Render README.Rmd to README.md")
  rmarkdown::render("README.Rmd")
  if (file.exists("README.html")) file.remove("README.html")

  # build the package
  message("# build the package")
  devtools::load_all()
  devtools::document()
  devtools::install(quick = TRUE,
                    upgrade = "never")

  # export the data as .csv
  loadRData <- function(fileName){
    #loads an RData file, and returns it
    load(fileName)
    get(ls()[ls() != "fileName"])
  }

  dataFiles <- list.files("data/", recursive = TRUE)
  for(f in dataFiles) {
    dataLoaded <- loadRData(paste0("data/", f))
    # rio::export(dataLoaded,
    #             file = paste0("../../../09 Shareable Data and Instruments/Database/KnologyFinEdStateSpending_",
    #                           gsub(".rda",
    #                                "",
    #                                f),
    #                           "_Data.csv"))
  }

  # clean and build website
  message("# clean and build website")
  pkgdown::clean_site()
  pkgdown::build_site()

  # copy icons
  message("# copy icons")
  iconDirectories <- c("docs/icons",
                       "docs/reference/icons",
                       "docs/articles/icons",
                       "docs/articles/codebooks/icons")
  for(iconD in iconDirectories) {
    print(iconD)
    dir.create(iconD)
    file.copy(from = c("pkgdown/icons/KnologyLogoStandard.png",
                       "pkgdown/icons/NEFE_logo-main.png"),
              to = paste0(iconD, "/"),
              overwrite = TRUE,
              recursive = FALSE,
              copy.mode = TRUE)
  }

  # adjust style things
  message("# adjust style things")
    # remove comma between logos
    message("  # remove comma between logos")
    docsFiles <- list.files("docs/", recursive = TRUE)
    docsFiles <- docsFiles[grepl(".html$", docsFiles)]
    for( f in docsFiles ){
      x <- readLines(paste0("docs/", f))
      x <- gsub('</a>, <a href="https://www.nefe.org">',
                '</a> <a href="https://www.nefe.org">',
                x)
      x <- gsub("</a>, <a href='https://www.nefe.org'>",
                '</a> <a href="https://www.nefe.org">',
                x)
      x <- gsub('</a> <a href="https://www.nefe.org">',
                '</a><br />Funded by <a href="https://www.nefe.org">',
                x)
      x <- gsub('></a>.</p>',
                '></a> </p>\n<p>© 2021. This work is licensed under a CC BY-NC-ND 4.0 license, unless otherwise noted.</p>',
                x)
      cat(x,
          file = paste0("docs/", f),
          sep = "\n")

      # check for placeholders
      message("  # check for placeholders")
      if(length(grep("\\?\\?\\?", x)) > 0) {
        placeholders <- grep("\\?\\?\\?", x)
        message(paste0("\n", f, ": You still have placeholders (as '???') in your website:\n"),
                x[placeholders])
      }
    }

    # remove content from sidebar
    message("  # remove content from sidebar")
    x <- readLines("docs/index.html")
      # remove Library from sidebar
      x <- gsub('<h2>License</h2>',
                '<!-- <h2>License</h2> -->',
                x)
      x <- gsub('<li><a href="LICENSE.html">Full license</a></li>',
                '<!-- <li><a href="LICENSE.html">Full license</a></li> -->',
                x)
      x <- gsub('<li><small><a href="https://www.r-project.org/Licenses/GPL-3">GPL-3</a></small></li>',
                '<!-- <li><small><a href="https://www.r-project.org/Licenses/GPL-3">GPL-3</a></small></li> -->',
                x)

      # remove Developers from sidebar
      message("  # remove Developers from sidebar")
      x <- gsub('<h2>Developers</h2>',
                '<!-- <h2>Developers</h2> -->',
                x)
      x <- gsub('^<a href="https://www.knology.org"><img src="icons/KnologyLogoStandard.png" height="50"></a> <br><small class="roles"> Author, maintainer </small>  </li>$',
                '<!-- <a href="https://www.knology.org"><img src="icons/KnologyLogoStandard.png" height="50"></a> <br><small class="roles"> Author, maintainer </small> -->  </li>',
                x)
      x <- gsub('^<a href="https://www.nefe.org"><img src="icons/NEFE_logo-main.png" height="50"></a> <br><small class="roles"> Funder </small>  </li>$',
                '<!-- <a href="https://www.nefe.org"><img src="icons/NEFE_logo-main.png" height="50"></a> <br><small class="roles"> Funder </small> -->  </li>',
                x)
    cat(x,
        file = "docs/index.html",
        sep = "\n")

    x <- readLines("docs/authors.html")
    authorsStart <- grep("<h1>Authors", x)-1
    authorsEnd <- grep("Funder.",x)+6
    cat(c(x[0:authorsStart],x[authorsEnd:length(x)]),
        file = "docs/authors.html",
        sep = "\n")
}

# now execute the function
system.time(
  pkgdown_build_site_Custom()
)
