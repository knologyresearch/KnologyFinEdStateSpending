

#' Make lag variables from long datasets
#'
#' @param x Data, including \code{id_col}, \code{time}, and variables to be lagged
#' @param k Number of lag periods
#' @param id_col String to identify column with units of analysis
#' @param time String to identify column with units of time
#'
#' @return x plus lagged variables
#' @export
#'
#' @examples NA
makelags <- function(x, k, id_col, time) {
  # capture columns that need lags
  lag_cols <- colnames(x)[!colnames(x) %in% id_col &
                            !colnames(x) %in% time]

  for(t in 1:k) {
    # make L1, L2, ...
    x[[paste0("L", t)]] <- x[[time]] - t

    # for each column needing lags
    for(lc in 1:length(lag_cols)) {
      # for each row
      for(r in 1:nrow(x)) {
        # check if the time is the minimum for the id_col
        if(x[[time]][r] == min(x[[time]][x[[id_col]] == x[[id_col]][r]])) {
          break
        }

        # give L1, L2, ... suffixes to lagged columns
        # find the row where the original lagged column has the same
        # id_col and the time equals the lagged time in L1, L2, ...
        x[[paste0(colnames(x[ , lag_cols[lc]]), "_L", t)]][r] <-
          x[[lag_cols[lc]]][x[[id_col]] == x[[id_col]][r] &
                              x[[time]] == x[[paste0("L", t)]][r]]
      }
    }
  }

  return(x)
}





#' Adjust inconsistent items to a single scale
#'
#' @param v Vector whose values will be compressed or expanded
#' @param min_v Minimum allowed value for v
#' @param max_v Maximum allowed value for v
#' @param min_t Minimum allowed value for target scale
#' @param max_t Maximum allowed value for target scale
#'
#' @return v
#' @export
#'
#' @examples NA
scaleupdown <- function(v, min_v, max_v, min_t, max_t) {
  # measure length of scale vs. target scale
  vLength <- max_v - min_v
  tLength <- max_t - min_t

  # decrease min_v to 0
  v <- v - min_v

  # multiply v to match target
  v <- v * (tLength / vLength)

  # increase min_v to min_t
  v <- v + min_t

  return(v)
}





#' Raking of replicate weight design with "partial = TRUE"
#'
#' This is survey::rake() but with "partial = TRUE" inserted into postStratify()
#'
#' @param design A survey object
#' @param sample.margins list of formulas or data frames describing sample margins, which must not contain missing values
#' @param population.margins list of tables or data frames describing corresponding population margins
#' @param control maxit controls the number of iterations. Convergence is declared if the maximum change in a table entry is less than epsilon. If epsilon<1 it is taken to be a fraction of the total sampling weight.
#' @param compress If design has replicate weights, attempt to compress the new replicate weight matrix? When NULL, will attempt to compress if the original weight matrix was compressed
#'
#' @return design
#' @export
#'
#' @examples NA
rakePartial <- function (design, sample.margins, population.margins, control = list(maxit = 10,
                                                                     epsilon = 1, verbose = FALSE), compress = NULL)
{
  if (!missing(control)) {
    control.defaults <- formals(rakePartial)$control
    for (n in names(control.defaults)) if (!(n %in% names(control)))
      control[[n]] <- control.defaults[[n]]
  }
  is.rep <- inherits(design, "svyrep.design")
  if (is.rep && is.null(compress))
    compress <- inherits(design$repweights, "repweights_compressed")
  if (is.rep)
    design$degf <- NULL
  if (length(sample.margins) != length(population.margins))
    stop("sample.margins and population.margins do not match.")
  nmar <- length(sample.margins)
  if (control$epsilon < 1)
    epsilon <- control$epsilon * sum(weights(design, "sampling"))
  else epsilon <- control$epsilon
  strata <- lapply(sample.margins, function(margin) if (inherits(margin,
                                                                 "formula")) {
    mf <- model.frame(margin, data = design$variables, na.action = na.fail)
  })
  allterms <- unlist(lapply(sample.margins, all.vars))
  ff <- formula(paste("~", paste(allterms, collapse = "+"),
                      sep = ""))
  oldtable <- survey::svytable(ff, design)
  if (control$verbose)
    print(oldtable)
  oldpoststrata <- design$postStrata
  iter <- 0
  converged <- FALSE
  while (iter < control$maxit) {
    design$postStrata <- NULL
    for (i in 1:nmar) {
      design <- postStratifyPartial(design, strata[[i]], population.margins[[i]],
                             compress = FALSE, partial = TRUE)
    }
    newtable <- survey::svytable(ff, design)
    if (control$verbose)
      print(newtable)
    delta <- max(abs(oldtable - newtable))
    if (delta < epsilon) {
      converged <- TRUE
      break
    }
    oldtable <- newtable
    iter <- iter + 1
  }
  rakestrata <- design$postStrata
  if (!is.null(rakestrata)) {
    class(rakestrata) <- "raking"
    design$postStrata <- c(oldpoststrata, list(rakestrata))
  }
  design$call <- sys.call()
  if (is.rep && compress)
    design$repweights <- survey::compressWeights(design$repweights)
  if (is.rep)
    design$degf <- degf(design)
  if (!converged)
    warning("Raking did not converge after ", iter, " iterations.\n")
  return(design)
}

#' Post-stratify a survey with idiosyncratic correction
#'
#' This is survey::postStratify() but with one line commented out in order to
#' ensure that the match() to create the index lines up properly with data
#'
#' @param design A survey design
#' @param strata A formula or data frame of post-stratifying variables, which must not contain missing values.
#' @param population A table, xtabs or data.frame with population frequencies
#' @param partial if TRUE, ignore population strata not present in the sample
#' @param ... arguments for future expansion
#'
#' @return design
#' @export
#'
#' @examples NA
postStratifyPartial <- function (design, strata, population, partial = FALSE, ...)
{
  if (inherits(strata, "formula")) {
    mf <- substitute(model.frame(strata, data = design$variables,
                                 na.action = na.fail))
    strata <- eval.parent(mf)
  }
  strata <- as.data.frame(strata)
  sampletable <- xtabs(I(1/design$prob) ~ ., data = strata)
  sampletable <- as.data.frame(sampletable)
  if (inherits(population, "table"))
    population <- as.data.frame(population)
  else if (is.data.frame(population))
    population$Freq <- as.vector(population$Freq)
  else stop("population must be a table or dataframe")
  if (!all(names(strata) %in% names(population)))
    stop("Stratifying variables don't match")
  nn <- names(population) %in% names(strata)
  if (sum(!nn) != 1)
    stop("stratifying variables don't match")
  names(population)[which(!nn)] <- "Pop.Freq"
  both <- merge(sampletable, population, by = names(strata),
                all = TRUE)
  samplezero <- both$Freq %in% c(0, NA)
  popzero <- both$Pop.Freq %in% c(0, NA)
  # both <- both[!(samplezero & popzero), ]
  if (any(onlysample <- popzero & !samplezero)) {
    print(both[onlysample, ])
    stop("Strata in sample absent from population. This Can't Happen")
  }
  if (any(onlypop <- samplezero & !popzero)) {
    if (partial) {
      both <- both[!onlypop, ]
      warning("Some strata absent from sample: ignored")
    }
    else {
      print(both[onlypop, ])
      stop("Some strata absent from sample: use partial=TRUE to ignore them.")
    }
  }
  reweight <- both$Pop.Freq/both$Freq
  both$label <- do.call("interaction", list(both[, names(strata)]))
  designlabel <- do.call("interaction", strata)
  index <- match(designlabel, both$label)
  attr(index, "oldweights") <- 1/design$prob
  design$prob <- design$prob/reweight[index]
  attr(index, "weights") <- 1/design$prob
  design$postStrata <- c(design$postStrata, list(index))
  design$call <- sys.call(-1)
  design
}



# starting point for data documentation, print to console
# cat(paste0("#'   \\item{", colnames(SHEDAY[ , 1001:1143]), "}{", lapply(SHEDAY[ , 1001:1143], function(x) attributes(x)$label), "}", collapse = "\n"))
