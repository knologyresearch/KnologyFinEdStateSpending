## code to prepare `OtherFinEdActivities` dataset goes here

OtherFinEdActivities <- rio::import("../../../02 Data/Data/01 Spending Data/Other Programs/State Programs_2020 04 09.xlsx")

# remove unnecessary columns
OtherFinEdActivities <-
  OtherFinEdActivities[ , c(1:13, 17)]

# reorder the factor
OtherFinEdActivities$State <- factor(OtherFinEdActivities$State,
                                          levels =
                                            c("Not asked", "REFUSED",

                                              "Alabama", "Alaska", "Arizona", "Arkansas", "California",
                                              "Colorado", "Connecticut", "Delaware", "Florida", "Georgia",
                                              "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa",
                                              "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland",
                                              "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri",
                                              "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey",
                                              "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio",
                                              "Oklahoma", "Oregon", "Pennsylvania",  "Rhode Island", "South Carolina",
                                              "South Dakota", "Tennessee", "Texas", "Utah", "Vermont",
                                              "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming",

                                              "American Samoa", "District of Columbia", "Guam", "Puerto Rico", "Virgin Islands"))
attributes(OtherFinEdActivities$State)$label <-
  "State"
attributes(OtherFinEdActivities$State) <-
  attributes(OtherFinEdActivities$State)[c(1,3,2)]

# # ddd state abbreviations
# OtherFinEdActivities$StateAbb <-
#   state.abb[match(OtherFinEdActivities$State, state.name)]

# metadata for codebook
codebook::var_label(OtherFinEdActivities) <- list(
  State = "U.S. State",
  StateProgram = "Name of financial education program",
  StateProgramType = "Type of financial education program",
  StateEntity = "Institution behind financial education program",
  StatePartners = "Partner organizations",
  URLStateProgram = "URL for financial education program",
  URLStateProgramSecondary = "Other URLs for financial education program",
  StateProgramDetails = "Brief description of financial education program",
  StateProgramCosts = "Associated costs",
  IntendedAgeGroup = "Intended age group",
  ProgramNumberParticipantsAnnual = "Annual number of participants",
  YearsOfOperation = "Years of program operation",
  YearsofOperationSource = "Citation for years of program operation",
  Notes = "Notes"
)

codebook::metadata(OtherFinEdActivities)$name <- "Other financial education activities"
codebook::metadata(OtherFinEdActivities)$description <- "This dataset provides a snapshot of financial education activities we found in 2019 when looking at lists of resources curated by state boards of education. Specifically, this dataset features state-funded programs, even if the providers were non-profits instead of public schools. It offers users a rough sketch of the financial education landscape that can be factored into calculating states’ spending on K-12 education programs."
codebook::metadata(OtherFinEdActivities)$identifier <- "https://dx.doi.org/...."
codebook::metadata(OtherFinEdActivities)$creator <- "Knology; National Endowment for Financial Education"
codebook::metadata(OtherFinEdActivities)$citation <- "Dwyer, Joseph de la Torre, Field, Shaun, Flinner, Kate, Attaway, Elizabeth, Norlander, Rebecca, and LaMarca, Nicole. (2020). KnologyFinEdStateSpending."
codebook::metadata(OtherFinEdActivities)$url <- "https://gitlab.com/knologyresearch/KnologyFinEdStateSpending"
codebook::metadata(OtherFinEdActivities)$datePublished <- "NA"
codebook::metadata(OtherFinEdActivities)$temporalCoverage <- "2019 - 2020"
codebook::metadata(OtherFinEdActivities)$spatialCoverage <- "U.S. States"

usethis::use_data(OtherFinEdActivities, overwrite = TRUE)
