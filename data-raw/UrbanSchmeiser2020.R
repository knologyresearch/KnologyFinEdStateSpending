## code to prepare `UrbanSchmeiser2020` dataset goes here

UrbanSchmeiser2020 <- rio::import("../../../02 Data/External Reference Data/UrbanC_Policies_Panel_2020 07 27.xlsx")

# change to title case
re_from <- "\\b([[:lower:]])([[:lower:]]+)"
UrbanSchmeiser2020$state_name <-
  gsub(re_from,
       "\\U\\1\\L\\2" ,
       tolower(UrbanSchmeiser2020$state_name),
       perl=TRUE)
UrbanSchmeiser2020$state_name[
  UrbanSchmeiser2020$state_name == "District Of Columbia"] <-
  "District of Columbia"

# metadata for codebook
codebook::var_label(UrbanSchmeiser2020) <- list(
  state_abbrev = "State Abbreviation",
  state_name = "State Name",
  state_fips = "ANSI State FIPS Code",
  grad_year = "High school graduation class",
  finEd_treated = "Financial education required"
)


codebook::metadata(UrbanSchmeiser2020)$name <- "State High School Graduation Requirement Data"
codebook::metadata(UrbanSchmeiser2020)$description <- "State High School Graduation Requirement Data (1970-2024)"
codebook::metadata(UrbanSchmeiser2020)$identifier <- "https://dx.doi.org/...."
codebook::metadata(UrbanSchmeiser2020)$creator <- "Financial Industry Regulatory Authority, Carly Urban, Maximilian Schmeiser, Kyle Musser"
codebook::metadata(UrbanSchmeiser2020)$citation <- "Urban, Carly. (2020). State High School Graduation Requirement Data (1970-2024). https://www.montana.edu/urban/Policies_Panel.xlsx"
codebook::metadata(UrbanSchmeiser2020)$url <- "https://www.montana.edu/urban/Policies_Panel.xlsx"
codebook::metadata(UrbanSchmeiser2020)$datePublished <- "2020"
codebook::metadata(UrbanSchmeiser2020)$temporalCoverage <- "1970 - 2024"
codebook::metadata(UrbanSchmeiser2020)$spatialCoverage <- "U.S. States"


usethis::use_data(UrbanSchmeiser2020, overwrite = TRUE)
