
<!-- README.md is generated from README.Rmd. Please edit that file -->

# A Database of Financial Education Interventions by State and Year

<!-- badges: start -->

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/knologyresearch%2FKnologyFinEdStateSpending/master?urlpath=rstudio)
[![Lifecycle:
maturing](https://img.shields.io/badge/lifecycle-maturing-blue.svg)](https://www.tidyverse.org/lifecycle/#maturing)
<!-- [![Lifecycle: maturing](https://img.shields.io/badge/lifecycle-maturing-blue.svg)](https://www.tidyverse.org/lifecycle/#maturing) -->
<!-- [![CRAN Status](https://www.r-pkg.org/badges/version/pkgdown)](https://cran.r-project.org/package=pkgdown) -->
[![pipeline
status](https://gitlab.com/knologyresearch/KnologyFinEdStateSpending/badges/master/pipeline.svg)](https://gitlab.com/knologyresearch/KnologyFinEdStateSpending/-/commits/master)
<!-- [![R build status](https://github.com/r-lib/pkgdown/workflows/R-CMD-check/badge.svg)](https://github.com/r-lib/pkgdown/actions) -->
<!-- [![coverage report](https://gitlab.com/knologyresearch/KnologyFinEdStateSpending/badges/master/coverage.svg)](https://gitlab.com/knologyresearch/KnologyFinEdStateSpending/-/commits/master) -->
<!-- [![Codecov test coverage](https://codecov.io/gh/r-lib/pkgdown/branch/master/graphs/badge.svg)](https://codecov.io/gh/r-lib/pkgdown?branch=master) -->
<!-- badges: end -->

### About the Database

From 2019 to 2020, Knology built an extensive database as part of our
research into financial education spending at the state level, as well
as financial health outcomes for individuals living in those states. For
a detailed overview of this database, please see [this
article](https://knology.org/Financial_Education???).

<!-- This repository contains the data and code for our paper: -->

<!-- > Authors, (YYYY). _Title of your paper goes here_. Name of journal/book <https://doi.org/xxx/xxx> -->

<!-- ### How to cite -->

<!-- Please cite this R package and database as: -->

<!-- > Knology (2021). KnologyFinEdStateSpending: A Database of Financial Education Activities in the -->

<!--   U.S. by State. R package version 0.0.0.9001. Accessed 13 Apr 2021. Online at <https://gitlab.com/knologyresearch/KnologyFinEdStateSpending> -->

### How to download or install

You can download the compendium as a zip from this URL:

> <https://gitlab.com/knologyresearch/KnologyFinEdStateSpending/-/archive/master/KnologyFinEdStateSpending-master.zip>

Or you can install this compendium as an R package,
KnologyFinEdStateSpending, from GitLab with:

``` r
# install.packages("remotes")
remotes::install_gitlab("knologyresearch/KnologyFinEdStateSpending")
```

### Licenses

**Text and figures :** [CC
BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/)

**Code :** [GPL-3](https://www.r-project.org/Licenses/GPL-3)

**Data :** [CC-0](http://creativecommons.org/publicdomain/zero/1.0/)
attribution requested in reuse

**Full License :** [LICENSE](LICENSE.md)

### Contributions

We welcome contributions from everyone. Before you get started, please
see our [contributor guidelines](CONTRIBUTING.md). Please note that this
project is released with a [Contributor Code of Conduct](CONDUCT.md). By
participating in this project you agree to abide by its terms.
